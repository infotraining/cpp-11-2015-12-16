#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <type_traits>
#include <array>

using namespace std;

using ID = int;  //typedef int ID;
using FuncPtr = int(*)(int);
using VectorInt = vector<int>;

template <typename Key>
using Dictionary = map<Key, string>;

void foo(FuncPtr f)
{
	cout << f(10) << endl;
}

int square(int x)
{
	return x * x;
}

template <size_t N>
using Array = array<int, N>;

int main()
{
	foo(&square);

	Dictionary<int> dict = { {2, "two"}, {4, "four"}, {6, "six"} }; // map<int, string>

	dict.insert(make_pair(1, "one"));

	for (const auto& kv : dict)
	{
		cout << kv.first << " - " << kv.second << endl;
	}

	int arr0[255];
	Array<255> arr1 = { 1, 2, 3, 4, 5 };  // std::array<int, 255>
	arr1[0] = 1;

	system("PAUSE");
}