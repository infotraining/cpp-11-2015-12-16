#include <iostream>
#include <vector>

using namespace std;

struct X
{
	int value;

	X(int value) : value(value)
	{		
	}


	explicit X(const X& source) : value(source.value)
	{		
	}
};

vector<int> create_data()
{
	auto vec = vector<int>({1, 2, 3, 4});
	return vec;
}

int main()
{
	auto data = vector<int>{create_data()};

	auto i = 42;

	const auto& ri = i;

	vector<int> vec = { 1, 2, 3, 4, 5 };
	
	for (auto it = vec.cbegin(); it != vec.cend(); ++it)
	{
		auto value = *it;
		cout << value << " ";
	}
	cout << endl;

	X x1{ 10 };
	auto x2(x1);

	system("PAUSE");
}