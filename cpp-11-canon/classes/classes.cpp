#include <iostream>
#include <string>
#include <type_traits>
#include <vector>

using namespace std;

class NoCopyable
{
	int* ptr_;
public:
	NoCopyable(int value) : ptr_{ new int(value) }
	{}

	~NoCopyable()
	{
		delete ptr_;
	}

	NoCopyable(const NoCopyable&) = delete;
	NoCopyable& operator=(const NoCopyable&) = delete;
};

long gen_id()
{
	static long id_generator;

	return ++id_generator;
}

class Gadget
{
	int id_{ gen_id() };
	std::string name_ = "unknown" ;
	double price_{};

public:
	Gadget() = default;

	Gadget(const std::string& name) : Gadget(name, 9.99)
	{
	}

	Gadget(const std::string& name, double price) : name_(name), price_(price)
	{		
	}

	std::string name() const
	{
		return name_;
	}

	Gadget(Gadget&& source) : id_{ move(source.id_) }, name_(move(source.name_)), price_{move(source.price_)}
	{		
	}

	Gadget& operator=(Gadget&& source) 
	{
		if (this != &source)
		{
			id_ = move(source.id_);
			name_ = move(source.name_);
			price_ = move(source.price_);
		}

		return *this;
	}

	virtual int id() const
	{
		return id_;
	}

	virtual ~Gadget() = default;
};

class ModernGadget : public Gadget
{
public:
	//using Gadget::Gadget;
	ModernGadget(const std::string& name, double price) : Gadget(name, price)
	{}

	int id() const final override
	{
		return Gadget::id();
	}
};

class SuperGadget : public ModernGadget
{
	vector<string> features_;
public:
	SuperGadget(const std::string& name, double price) : ModernGadget(name, price)
	{}

	/*int id() const override
	{
		return Gadget::id();
	}*/
};

ostream& operator<<(ostream& out, const Gadget& g)
{
	out << "Gadget{id: " << g.id() << ", name: " << g.name() << "}";
	return out;
}

template <typename T>
typename enable_if<is_integral<T>::value>::type foo(T arg)
{
	cout << "integral_only(T)" << endl;
}

//void foo(double) = delete;

int main()
{
	NoCopyable nc1{ 10 };

	//NoCopyable nc2 = nc1;

	foo(10);
	foo(10L);

	short sx = 10;
	foo(sx);

	//double dx = 3.14;
	//foo(dx); 

	//float fx = 3.14F;
	//foo(fx);

	Gadget g1;

	cout << g1 << endl;

	Gadget g2{ "ipad" };

	cout << g2 << endl;

	system("PAUSE");
}