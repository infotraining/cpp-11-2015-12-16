#include <iostream>
#include <map>
#include <complex>
#include <type_traits>

using namespace std;

template <typename T>
auto multiply(T a, T b) -> decltype(a * b)
{
	return a * b;
}

int main()
{
	map<int, string> m1 = { { 1, "one" }, { 2, "two" } };

	decltype(m1) m2; // default ctor
	
	decltype(m1)::iterator it = m1.begin();

	auto m3 = m1; // copy ctor

	auto& item = m1[1];

	cout << multiply(1, 2) << endl;

	cout << multiply(complex<double>(1, 3), complex<double>(6, 3)) << endl;

	double dx = 6.7;
	auto&& data = dx;
		 
	static_assert(is_same<double&, decltype(data)>::value, "Error");

	system("PAUSE");
}