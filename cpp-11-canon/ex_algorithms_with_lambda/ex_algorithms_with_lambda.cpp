#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
	vector<int> vec(25);

	std::random_device rd;
	std::mt19937 mt{ rd() };
	std::uniform_int_distribution<int> uniform_dist{ 1, 30 };

	generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); });

	cout << "vec: ";
	for (const auto& item : vec)
		cout << item << " ";
	cout << endl;

	// 1a - wy�wietl parzyste
	auto is_even = [](int x) { return x % 2 == 0; };

	cout << "even: ";
	copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
			is_even);
	cout << endl;

	// 1b - wyswietl ile jest parzystych
	cout << "even count: " << count_if(vec.begin(), vec.end(), is_even) << endl;

	int eliminators[] = { 3, 5, 7 };
	// 2 - usu� liczby podzielne przez dowoln� liczb� z tablicy eliminators
	vec.erase(remove_if(vec.begin(), vec.end(), 
						[&eliminators](int x) {
							return any_of(begin(eliminators), end(eliminators),
								[x](int d) { return x % d == 0; });
						}), vec.end());

	cout << "vec after removal: ";
	for (const auto& item : vec)
		cout << item << " ";
	cout << endl;

	// 3 - tranformacja: podnie� liczby do kwadratu
	transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x; });

	// 4 - wypisz 5 najwiekszych liczb
	nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());

	cout << "5 greatest numbers: ";
	copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
	cout << endl;

	// 5 - policz wartosc srednia
	int sum = 0;
	for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x; });
	
	double avg = static_cast<double>(sum) / vec.size();

	// 6 - utw�rz dwa kontenery - 1. z liczbami mniejszymi lub r�wnymi �redniej, 2. z liczbami wi�kszymi od �redniej
	vector<int> gt_than_avg;
	vector<int> ls_eq_than_avg;

	partition_copy(vec.begin(), vec.end(),
		back_inserter(gt_than_avg), back_inserter(ls_eq_than_avg),
		[avg](int x) { return x > avg; });

	cout << "avg: " << avg << endl;
	cout << "gt_than_avg: ";
	copy(gt_than_avg.begin(), gt_than_avg.end(), ostream_iterator<int>(cout, " "));
	cout << endl;

	cout << "ls_eq_than_avg: ";
	copy(ls_eq_than_avg.begin(), ls_eq_than_avg.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	system("PAUSE");
}
