#include <iostream>
#include <vector>
#include <iterator>
#include <memory>
#include <assert.h>
#include <list>

using namespace std;

template <typename Container>
auto find_null(Container& container) -> decltype(begin(container))
{
	/*auto it = begin(container);
	for (; it != end(container); ++it)
	{
		if (*it == nullptr)
			return it;
	}

	return it;*/

	return find(begin(container), end(container), nullptr);
}

int main()
{

	// find_null: Given a container of pointers, return an
	// iterator to the first null pointer (or the end
	// iterator if none is found)

	// uncomment
	 vector<int*> ptrs = { new int{9}, new int{10}, NULL, new int{20}, nullptr, new int{23} };

	 auto where_null = find_null(ptrs);

	 assert(distance(begin(ptrs), where_null) == 2);

	 auto il = { make_shared<int>(10), shared_ptr<int>{}, make_shared<int>(3) };

	 auto where_null_sp = find_null(il);

	 assert(distance(il.begin(), where_null_sp) == 1);

	 system("PAUSE");
}
