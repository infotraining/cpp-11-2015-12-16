#include <iostream>
#include <cassert>

using namespace std;

template <typename Head, typename... Tail>
double sum(Head head, Tail... tail)
{
	return head + sum(tail...);
}

template <typename T>
double sum(T arg)
{
	return arg;
}

template <typename... Args>
double avg(Args... args)
{
	static_assert(sizeof...(args) > 0, "At leat one argument is required");

	return sum(args...) / sizeof...(args);
}

int main()
{
	//avg();

	assert(avg(1) == 1);

	assert(avg(1, 4) == 2.5);

	assert(avg(1, 2, 3, 4, 5) == 3);
}