#include <iostream>
#include <future>
#include <string>
#include <vector>

using namespace std;

int square(int x)
{
	cout << "square(" << x << ") has started..." << endl;
	this_thread::sleep_for(chrono::milliseconds(6000));
	cout << "square(" << x << ") has ended..." << endl;
	return x * x;
}

string download_file(string url)
{
	this_thread::sleep_for(chrono::milliseconds(3000));
	throw runtime_error("Bad url");
}

int main()
{
	future<int> fr1 = async(launch::async, square, 4);
	
	for (int i = 0; i < 10; ++i)
	{
		cout << "Main is working..." << endl;
		this_thread::sleep_for(chrono::milliseconds(100));
	}

	vector<future<int>> futures;

	for (int i = 3; i < 20; ++i)
		futures.push_back(async(launch::async, [i] { return square(i); }));

	int result = fr1.get();

	cout << "result: " << result << endl;

	for (auto& f : futures)
		cout << "result = " << f.get() << endl;

	cout << "\n\n";

	auto downloader = async(launch::async, [] { return download_file("text.txt"); });

	try
	{
		cout << downloader.get() << endl;
	}
	catch (const runtime_error& e)
	{
		cout << "Exception: " << e.what() << endl;
	}

	system("PAUSE");
}
