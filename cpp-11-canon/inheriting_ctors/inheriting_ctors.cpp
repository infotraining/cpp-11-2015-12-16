#include <iostream>
#include <set>
#include <functional>

template <typename T, typename Comparer = std::less<T>, typename Alloc = std::allocator<T>>
struct IndexableSet : std::set<T, Comparer, Alloc>
{
	using SetType = std::set<T, Comparer, Alloc>;
	using size_type = typename SetType::size_type;

	// inheriting ctors
	//using std::set<T, Comparer, Alloc>::set;

	const T& at(size_type index) const
	{
		if (index >= SetType::size())
		{
			throw std::out_of_range{ "IndexableSet::operator[] out of range" };
		}

		return this->operator[](index);
	}

	const T& operator[](size_type index) const
	{
		return *std::next(SetType::begin(), index);
	}
};

int main()
{
	using namespace std;

	//IndexableSet<int> indexed_set = { 5, 1, 65, 234, 66, 234, 66 };

	//cout << "3rd item: " << indexed_set[2] << endl;
}