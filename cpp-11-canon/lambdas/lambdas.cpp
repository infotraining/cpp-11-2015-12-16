#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <functional>
#include <set>

using namespace std;

class MagicLambda_24242
{
public:
	int operator()(int a, int b) const { return a * b; };
};

class MagicLambda_23424
{
	const int threshold;
	int& sum;
public:
	MagicLambda_23424(int threshold, int& sum) : threshold{ threshold }, sum{ sum }
	{}

	void operator() (int item) const 
	{
		if (item > threshold) sum += item;
	}
};

inline int square(int x)
{
	return x * x;
}

int counter = 0;

std::function<int()> create_id_gen()
{
	int seed = 0;

	return[seed] () mutable { return ++seed; };
}

function<vector<int>()> create_vector_gen()
{
	std::vector<int> vec = { 1, 2, 3, 4, 5, 6, 7 };

	return [vec]() mutable {
		for (auto& item : vec)
			++item;
		return vec;
	};
}

template <typename Container>
void print(const Container& cont, const std::string& prefix)
{
	cout << prefix << ": [ ";
	for (const auto& item : cont)
		cout << item << ", ";
	cout << "]" << endl;
}

int main()
{
	auto multiply = [](int a, int b) { return a * b; };

	auto desc_multiply = MagicLambda_24242{};

	cout << "multiply(9, 2) = " << multiply(9, 2) << endl;

	auto complicated_logic = [](int threshold) -> string {
		if (threshold > 10)
			return "Greater than 10";
		else
			return string("Less or equal than 10");
	};

	cout << complicated_logic(19) << endl;

	int(*ptr_fun)(int, int) = [](int a, int b) { return a + b; };

	vector<int> numbers = { 1, 2, 3, 4, 5, 6, 7 }; 

	transform(numbers.begin(), numbers.end(), numbers.begin(),
			  [](int x) { return x * x; });

	//transform(numbers.begin(), numbers.end(), numbers.begin(), square);

	int threshold = 30;

	auto where_gt_30 = find_if(numbers.begin(), numbers.end(),
							   [=](int item) { return item > threshold; });

	int sum = 0;
	for_each(numbers.begin(), numbers.end(), 
			 [threshold, &sum](int item) { if (item > threshold) sum += item; });

	auto vec_gen = create_vector_gen();

	auto set1 = vec_gen();
	print(set1, "set1");

	auto set2 = vec_gen();
	print(set2, "set2");

	auto set3 = vec_gen();
	print(set3, "set3");

	/*auto comparer = [](int a, int b) { return a > b; };
	set<int, decltype(comparer)> set_numbers(comparer);

	set_numbers.insert({ 1, 2, 3, 65, 345, 2, 345, 25, 66 });

	print(set_numbers, "set_numbers");*/

	auto gen_id1 = create_id_gen();

	cout << "id_gen1: " << gen_id1() << " " << gen_id1() << " " << gen_id1() << endl;

	auto gen_id2 = create_id_gen();

	cout << "id_gen2: " << gen_id2() << " " << gen_id2() << " " << gen_id2() << endl;

	system("PAUSE");
}