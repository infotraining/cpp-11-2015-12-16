#include <iostream>
#include <string>
#include <tuple>
#include <map>
#include <functional>

using namespace std;

int factorial(int x)
{
	if (x == 1)
		return 1;

	return factorial(x - 1) * x;
}

template <typename ResultType, typename... Args>
std::function<ResultType(Args...)> memoize(ResultType(*func)(Args...))
{
	std::map<std::tuple<Args...>, ResultType> cache;

	return [=](Args... args) mutable
	{
		std::tuple<Args...> args_as_tuple(args...);

		if (cache.find(args_as_tuple) == cache.end())
		{
			std::cout << "Computed for a first time!" << std::endl;
			auto result = func(args...);
			cache[args_as_tuple] = result;
			return result;
		}

		std::cout << "Returning cached value!" << std::endl;

		return cache[args_as_tuple];
	};
}

int main()
{
	auto mem_factorial = memoize(factorial);

	cout << "8! = " << mem_factorial(8) << endl;
	cout << "8! = " << mem_factorial(8) << endl;

	std::cout << std::endl;

	cout << "6! = " << mem_factorial(6) << endl;
	cout << "6! = " << mem_factorial(6) << endl;

	std::cout << std::endl;

	cout << "8! = " << mem_factorial(8) << endl;
	cout << "6! = " << mem_factorial(6) << endl;

	std::cout << std::endl;

	system("PAUSE");
}