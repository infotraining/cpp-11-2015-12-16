#define D_SCL_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

class Bitmap
{
	string name_ = "unknown";
	char* image_ = nullptr;

public:
	Bitmap(const string& name, char fill_character = '+') : name_(name)
	{
		cout << "Ctor Bitmap(" << name_ << ")" << endl;
		image_ = new char[20];
		fill(image_, image_+19, fill_character);
		image_[19] = 0;
	}

	Bitmap(const Bitmap& source) : name_(source.name_)
	{
		cout << "CopyCtor Bitmap(" << name_ << ")" << endl;
		image_ = new char[20];
		copy(source.image_, source.image_ + 20, image_);
	}

	// move ctor
	Bitmap(Bitmap&& source) /*noexcept*/ : name_( move(source.name_) ), image_{source.image_}
	{
		cout << "MoveCtor Bitmap(" << name_ << ")" << endl;
		source.image_ = nullptr;
	}

	// move assignment
	Bitmap& operator=(Bitmap&& source) /*noexcept*/
	{
		if (this != &source)
		{
			cout << "Move operator=(Bitmap&&: " << source.name_ << ")" << endl;

			name_ = move(source.name_);
			image_ = source.image_;
			source.image_ = nullptr;
		}

		return *this;
	}

	Bitmap& operator=(const Bitmap& source)
	{
		if (this != &source)
		{
			cout << "Copy operator=(const Bitmap&: " << source.name_ << ")" << endl;

			name_ = source.name_;
			copy(source.image_, source.image_ + 20, image_);			
		}

		return *this;
	}

	~Bitmap()
	{
		cout << "Dtor Bitmap(" << name_ << ")" << endl;
		delete[] image_;
	}

	void draw() const	
	{
		cout << "Drawing " << name_ << ": ";

		if (image_)
			cout << image_ << endl;
		else
			cout << "nullptr" << endl;
	}
};

Bitmap create_bitmap()
{
	static int gen_id;

	Bitmap bmp{ "bmp_from_factory" + to_string(++gen_id), '*' };

	return bmp;
}

int main()
{
	{
		Bitmap bmp1{ "bmp1" };

		bmp1.draw();

		Bitmap bmp2 = move(bmp1);

		cout << "\nAfter move: " << endl;

		bmp1.draw();

		Bitmap bmp3 { "bmp2" };

		bmp3.draw();

		bmp3 = bmp2;

		bmp3.draw();

		Bitmap bmp4 = create_bitmap();

		bmp4.draw();

		cout << "\nVector images: " << endl;

		vector<Bitmap> vec_images;

		vec_images.push_back(create_bitmap());
		vec_images.push_back(create_bitmap());
		vec_images.push_back(create_bitmap());
		vec_images.push_back(Bitmap("image", '$'));

		for (const auto& img : vec_images)
			img.draw();
	}

	system("PAUSE");
}