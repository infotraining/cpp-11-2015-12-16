#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <assert.h>

using namespace std;

class Data
{
	int value_;

public:
	Data(int value) : value_{value}
	{
		cout << "Data(" << value_ << ")" << endl;
	}

	~Data()
	{
		cout << "~Data(" << value_ << ")" << endl;
	}

	int data() const
	{
		return value_;
	}
};

//namespace LegacyCode
//{
//	Data* create_data(int value)
//	{
//		// factory logic
//		return new Data(value);
//	}
//}

unique_ptr<Data> create_data(int value)
{
	auto result = make_unique<Data>(value);

	// logic

	return result;
}

int main()
{
	{
		unique_ptr<Data> up1 = make_unique<Data>(10);
		cout << "up1->data() = " << up1->data() << endl;

		unique_ptr<Data> up2 = move(up1);
		cout << "up2->data() = " << up2->data() << endl;
		assert(up1.get() == nullptr);

		vector<unique_ptr<Data>> vec_ptrs;

		vec_ptrs.push_back(make_unique<Data>(20));
		vec_ptrs.push_back(create_data(30));
		vec_ptrs.push_back(move(up2));

		auto up3 = create_data(50);

		cout << "\nVec: ";
		for (const auto& ptr : vec_ptrs)
			cout << "Item: " << ptr->data() << endl;
	}

	system("PAUSE");
}