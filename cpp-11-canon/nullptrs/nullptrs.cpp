#include <iostream>

using namespace std;

void foo(int* ptr)
{
	cout << "foo(int*)" << endl;
	if (ptr)
		cout << "*ptr = " << *ptr << endl;
}

void foo(nullptr_t)
{
	cout << "foo(nullptr)" << endl;
}

void foo(int number)
{
	cout << "foo(int: " << number << ")" << endl;
}

int main()
{
	int* ptr1 = nullptr;
	int* ptr2{ nullptr };
	int* ptr3{};

	foo(ptr1);
	foo(ptr2);
	foo(ptr3);

	foo(nullptr);

	system("PAUSE");
}