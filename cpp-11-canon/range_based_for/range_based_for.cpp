#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <memory>

using namespace std;

int main()
{
	vector<int> numbers = { 1, 2, 3, 4, 5, 6 };

	cout << "iteracja po vector<int>: ";
	for (const auto& item : numbers)
	{
		cout << item << " ";
	}
	cout << endl;

	/*for (auto it = numbers.begin(); it != numbers.end(); ++it)
	{
		const auto& item = *it;

		cout << item << " ";
	}*/

	int tab[10] = { 1, 2, 3, 4, 5, 6 };

	cout << "iteracja po int tab[10]: ";
	for (auto item : tab)
	{
		cout << item << " ";
	}
	cout << endl;

	/*for (auto it = begin(tab); it != end(tab); ++it)
	{
		int item = *it;

		cout << item << " ";
	}*/
	
	// use-case1
	vector<string> words = { "one", "two", "three" };

	cout << "iteracja po vector<string>: ";
	for (auto&& w : words)
		cout << w << " ";
	cout << endl;

	// use-case2
	vector<shared_ptr<int>> ptrs = { make_shared<int>(10), make_shared<int>(20) };

	cout << "iteracja po vector<shared_ptr<int>>: ";
	for (const auto& ptr : ptrs)
	{
		cout << *ptr << " ";
	}
	cout << endl;
	
	cout << "iteracja po initializer list: ";
	for (auto item : { 1, 2, 3, 4, 5 })
	{
		cout << item << " ";
	}
	cout << endl;

	vector<bool> vec_bool = { 1, 0, 0, 0, 0, 1 };

	// special case
	for (auto&& bit : vec_bool)
	{
		bit.flip();
	}

	cout << "vec_bool: "<< endl;
	for (auto&& bit : vec_bool)
		cout << bit << " ";
	cout << endl;

	system("PAUSE");
}