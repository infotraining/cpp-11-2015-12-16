#include <iostream>
#include <cstdint>

using namespace std;

enum class Coffee : uint8_t; // forward declaration
 
void foo(Coffee); // function declaration

enum class Coffee : uint8_t { espresso = 1, latte = 2, cappucino = 4 };

void foo(Coffee coffee)
{
	switch (coffee)
	{
	case Coffee::latte:
		cout << "latte" << endl;
		break;
	case Coffee::cappucino:
		cout << "cappucino" << endl;
		break;
	case Coffee::espresso:
		cout << "espresso" << endl;
		break;
	}
}

template <typename T>
using UnderlyingType_t = typename underlying_type<T>::type;

int main()
{
	static_assert(is_same<uint8_t, UnderlyingType_t<Coffee>>::value, "Error");

	Coffee c1 = Coffee::latte;	

	unsigned char value_c1 = static_cast<unsigned char>(c1);
	cout << "value_c1 = " << static_cast<int>(value_c1) << endl;

	Coffee c2 = static_cast<Coffee>(199);
	cout << "c2 = " << static_cast<int>(c2) << endl;

	system("PAUSE");
}