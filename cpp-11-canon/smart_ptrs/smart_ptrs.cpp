#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <string>
#include <memory>

const char* get_line()
{
	static unsigned int count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file{};

	fopen_s(&file, file_name, "w");

	if (!file)
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for (size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}


// TO DO: RAII
void save_to_file_with_unique_ptr(const char* file_name)
{
	FILE* file{};

	fopen_s(&file, file_name, "w");

	//std::unique_ptr<FILE, int(*)(FILE*)> safe_file{ file, &fclose };

	auto file_closer = [](FILE* f) { fclose(f); };
	std::unique_ptr<FILE, decltype(file_closer)> safe_file{ file, file_closer };

	if (!safe_file)
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for (size_t i = 0; i < 100; ++i)
		fprintf(safe_file.get(), get_line());
}

void save_to_file_with_shared_ptr(const char* file_name)
{
	FILE* file{};

	fopen_s(&file, file_name, "w");

	std::shared_ptr<FILE> safe_file{ file, [](FILE* f) { if (f) fclose(f); } };

	if (!safe_file)
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for (size_t i = 0; i < 100; ++i)
		fprintf(safe_file.get(), get_line());
}

int main()
try
{
	std::unique_ptr<int[]> smart_array{ new int[100] };

	smart_array[2] = 3;

	//save_to_file("text.txt");
	save_to_file_with_unique_ptr("text.txt");
}
catch (const std::exception& e)
{
	std::cout << e.what() << std::endl;

	std::cout << "Press a key..." << std::endl;
	std::string temp;
	std::getline(std::cin, temp);
}
