#include <iostream>
#include <string>
#include <functional>

using namespace std;

void foo(int x)
{
	cout << "foo(x: " << x << ")" << endl;
}

class Foo
{
public:
	void operator()(int x)
	{
		cout << "Foo(x: " << x << ")" << endl;
	}
};

int main()
{
	void(*ptr_fun)(int) = &foo;

	ptr_fun(10);

	Foo f1;

	f1(10);

	function<void(int)> std_f;

	std_f = &foo;

	std_f(10);

	std_f = f1;

	std_f(20);

	std_f = [](int x) { cout << "Lambda: " << x << endl; };

	std_f(30);

	system("PAUSE");
}