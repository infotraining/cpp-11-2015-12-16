#include <iostream>
#include <thread>
#include <string>

using namespace std;

void task1(int id, int delay)
{
	for (int i = 0; i < 10; ++i)
	{
		cout << "THD#" << id << ": " << i << endl;
		this_thread::sleep_for(chrono::milliseconds(delay));
	}

	cout << "THD#" << id << " has finished..." << endl;
}

void task2(int id, int& arg, string& prefix, int delay)
{
	for (int i = 0; i < 10; ++i)
	{
		cout << prefix << "#" << id << ": " << ++arg << endl;
		this_thread::sleep_for(chrono::milliseconds(delay));
	}

	cout << "Task#" << id << " has finished..." << endl;
}

void may_throw(int arg)
{
	cout << "may_throw(" << arg << ")" << endl;

	if (arg == 13)
		throw runtime_error("Error#13");
}

class ThreadGuard
{
public:
	enum class DtorAction { join, detach };

	ThreadGuard(DtorAction action, thread&& thd) : thd_{ move(thd) }, action_{ action }
	{}

	template <typename... T>
	ThreadGuard(DtorAction action, T&&... args) : action_{ action }, thd_{ forward<T>(args)... }
	{}

	ThreadGuard(const ThreadGuard&) = delete;
	ThreadGuard& operator=(const ThreadGuard&) = delete;

	//ThreadGuard(ThreadGuard&&) = default;
	//ThreadGuard& operator=(ThreadGuard&&) = default;

	~ThreadGuard()
	{
		if (action_ == DtorAction::join)
		{
			if (thd_.joinable())
				thd_.join();
		}
		else
			thd_.detach();
	}

	thread& get()
	{
		return thd_;
	}

private:
	thread thd_;
	DtorAction action_;
};

void local_function()
{
	int x = 0;
	string text("THD");

	ThreadGuard guard1{ ThreadGuard::DtorAction::detach, thread{ &task1, 1, 200 } };

	//ThreadGuard guard2{thread{&task2, 1, ref(x), ref(text), 300}, ThreadGuard::DtorAction::join};
	ThreadGuard guard2{ ThreadGuard::DtorAction::join, &task2, 1, ref(x), ref(text), 300 };


	//ThreadGuard mvd_guard = move(guard2);

	for (int i = 0; i < 10; ++i)
	{
		may_throw(i);
		this_thread::sleep_for(chrono::milliseconds(200));
	}

	cout << "END of local_function" << endl;
}

int main()
{
	try
	{
		local_function();
	}
	catch (const exception& e)
	{
		cout << "Caught exception: " << e.what() << endl;
	}

	this_thread::sleep_for(chrono::seconds(5));
}
