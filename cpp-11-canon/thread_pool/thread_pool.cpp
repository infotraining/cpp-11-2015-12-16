#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include "thread_safe_queue.hpp"

using namespace std;

using Task = std::function<void()>;

const nullptr_t END_OF_WORK{};

class ThreadPool
{
public:
	ThreadPool(size_t no_of_threads)
	{
		for (size_t i = 0; i < no_of_threads; ++i)
			threads_.emplace_back([this] { run(); });
	}

	ThreadPool(const ThreadPool&) = delete;
	ThreadPool& operator=(const ThreadPool&) = delete;

	~ThreadPool()
	{
		for (size_t i = 0; i < threads_.size(); ++i)
			submit(END_OF_WORK);

		for (auto& t : threads_)
			t.join();
	}

	void submit(Task t)
	{
		task_queue_.push(t);
	}

private:
	void run()
	{
		while (true)
		{
			Task task;

			task_queue_.wait_and_pop(task);

			if (task == END_OF_WORK)
				return;
			
			task();
		}
	}

	vector<thread> threads_;
	ThreadSafeQueue<Task> task_queue_;
};


void background_task(int id)
{
	cout << "BT#" << id << " starts..." << endl;
	this_thread::sleep_for(chrono::milliseconds(rand() % 3000));
	cout << "BT#" << id << " ends..." << endl;
}

int main()
{
	ThreadPool thread_pool{ 4 };

	thread_pool.submit([] { background_task(1); });
	thread_pool.submit([] { background_task(2); });

	for (int i = 3; i < 20; ++i)
		thread_pool.submit([i] { background_task(i); });

	system("PAUSE");
}