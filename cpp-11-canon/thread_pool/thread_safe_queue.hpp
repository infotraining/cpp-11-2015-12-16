#include <mutex>
#include <queue>
#include <condition_variable>

template <typename T>
class ThreadSafeQueue
{
	std::queue<T> queue_;
	std::mutex mtx_;
	std::condition_variable cv_queue_;

public:
	ThreadSafeQueue() = default;

	ThreadSafeQueue(const ThreadSafeQueue&) = delete;
	ThreadSafeQueue& operator=(const ThreadSafeQueue&) = delete;

	void push(const T& item)
	{
		std::lock_guard<std::mutex> lk{ mtx_ };
		queue_.push(item);
		cv_queue_.notify_one();
	}

	void wait_and_pop(T& item)
	{
		std::unique_lock<std::mutex> lk{ mtx_ };
		cv_queue_.wait(lk, [this] { return !queue_.empty(); });
		item = queue_.front();
		queue_.pop();
	}
};

