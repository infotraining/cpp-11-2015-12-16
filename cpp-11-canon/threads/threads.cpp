#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void background_work(int id, int sleep_interval)
{
	for (int i = 0; i < 5 * id; ++i)
	{
		cout << "BW#" << id << " is working... " << endl;
		this_thread::sleep_for(chrono::milliseconds(sleep_interval));
		// this_thread::sleep_for(1000ms); // C++14
	}
}

void may_throw()
{
	throw runtime_error("Error13");
}

enum class ThreadAction { joinable, detached };

class ThreadGuard
{
	thread& thd_;
	ThreadAction action_;
public:
	ThreadGuard(thread& thd, ThreadAction action = ThreadAction::joinable) : thd_{ thd }, action_{action}
	{}

	ThreadGuard(const ThreadGuard&) = delete;
	ThreadGuard& operator=(const ThreadGuard&) = delete;

	~ThreadGuard()
	{
		if (action_ == ThreadAction::joinable)
		{
			if (thd_.joinable())
				thd_.join();
		}
		else
			thd_.detach();
	}
};

int main()
{
	cout << "Hardware concurrency: " << thread::hardware_concurrency() << endl;

	thread thd0;

	cout << "thd0: " << thd0.get_id() << endl;

	thread thd1{ &background_work, 1, 1000 };
	thread thd2{ []{background_work(2, 500); } };

	thread thd3{ background_work, 4, 2000 };
	ThreadGuard thd_guard{ thd3 };
	//may_throw();

	thd0 = move(thd1);

	vector<thread> threads;
	threads.push_back(move(thd0));
	threads.push_back(move(thd2));
	threads.push_back(thread{ background_work, 4, 200 });

	for (auto& t : threads)
		t.join();

	//this_thread::sleep_for(chrono::milliseconds(10000));

	system("PAUSE");
}
