#include <iostream>
#include <string>
#include <tuple>
#include <algorithm>
#include <vector>
#include <numeric>

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& vec)
{
	auto min = *min_element(vec.begin(), vec.end());
	auto max = *max_element(vec.begin(), vec.end());
	auto avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

	return make_tuple(min, max, avg);
}

class Person
{
	int id;
	string fname;
	string lname;
public:
	Person(int id, string fname, string lname) : id(id), fname(fname), lname(lname)
	{		
	}

	bool operator==(const Person& p)
	{
		return tie(id, fname, lname) == tie(p.id, p.fname, p.lname);
	}
};

int main()
{
	int min, max;
	//double avg;

	//tuple<int&, int&, double&> ref_result{ min, max, avg };

	tie(min, max, ignore) = calc_stats({ 1, 2, 3, 5, 6 });

	//min = get<0>(result);
	//max = get<1>(result);
	//avg = get<2>(result);

	cout << "min: " << min << ", max: " << max << endl;

	system("PAUSE");
}