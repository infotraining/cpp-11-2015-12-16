#include <string>
#include <vector>
#include <iostream>
#include <memory>

using namespace std;

struct Point
{
	unsigned char x, y;

	Point(int x) : x{ x }, y{ x }
	{
	}

	Point(int x, int y) : x{ x }, y{ y }
	{		
	}

	static Point origin()
	{
		return	{ 0, 0 };
	}
};

class Container
{
	vector<int> items_;
public:
	Container()
	{
		cout << "Default ctor" << endl;
	}

	Container(int size) : items_(size)
	{		
	}

	Container(std::initializer_list<int> init_list)
	{
		items_.reserve(init_list.size());

		for (const auto& item : init_list)
			items_.push_back(item);
	}

	const vector<int>& data() const
	{
		return items_;
	}

	vector<int>::iterator begin()
	{
		return items_.begin();
	}

	vector<int>::const_iterator begin() const
	{
		return items_.cbegin();
	}

	vector<int>::iterator end()
	{
		return items_.end();
	}

	vector<int>::const_iterator end() const
	{
		return items_.cend();
	}
};

template <typename Container>
void print(const Container& cont, const std::string& prefix)
{
	cout << prefix << ": [ ";
	for (const auto& item : cont)
		cout << item << ", ";
	cout << "]" << endl;
}

int main()
{
	int a{ 5L }; // int a(5);
	int b(5.6);

	cout << "a = " << a << "; b = " << b << endl;

	int c{};  // int c = 0;

	int tab[10] = {}; // it works

	Point pt1 = { 1, 2656 };
	Point pt2 = { 5 };

	shared_ptr<int> ptr{new int(10)};

	vector<int> vec = { 1, 2, 3, 4, 5, 7 };

	vec.insert(vec.end(), { 10, 11, 23 });

	Container cont = { 1, 2, 3, 4, 5, 6 };

	print(cont, "cont");

	Container c2{ 10 };

	print(c2, "c2");
	

	vector<int> vec2{ 10, -1 };

	const Container c3{};

	auto it = c3.begin();

	print(c3, "c3");

	auto il = { 1, 2, 3, 4, 5, 6, 7 };
	print(il, "il");

	print(std::initializer_list<int>{ 1, 2, 3, 5}, "il2");

	system("PAUSE");
}