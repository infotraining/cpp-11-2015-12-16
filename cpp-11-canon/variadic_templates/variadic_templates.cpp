#include <iostream>
#include <string>
#include <memory>

using namespace std;

template <typename T, typename... Tail>
void print(const T& arg, const Tail&... params)
{
	cout << "Printing: " << arg << endl;

	print(params...);
}

void print()
{}

// my_make_unique
template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
	return unique_ptr<T>{ new T(forward<Args>(args)...)};
}

template <typename... Base>
struct Derived : public Base...
{

};

class Car
{};

class Boat
{};

using Amph = Derived<Car, Boat>;

int main()
{
	print(56, 234.0, "test", string("test_string"));

	unique_ptr<string> ptr1 = my_make_unique<string>("text");

	system("PAUSE");
}