#include <iostream>
#include <string>
#include <memory>

class Human
{
public:
	Human(const std::string& name) : name_(name)
    {
		std::cout << "Konstruktor Human(" << name_ << ")" << std::endl;
	}

	~Human()
	{
		std::cout << "Desktruktor ~Human(" << name_ << ")" << std::endl;
	}

	void set_partner(std::weak_ptr<Human> partner)
	{
		partner_ = partner;
	}

    std::weak_ptr<Human> partner() const
    {
        return partner_;
    }

    void description() const
    {
        std::cout << "My name is " << name_ << std::endl;

        auto sp_partner = partner_.lock();
        if (sp_partner)
        {
            std::cout << "My partner is " << sp_partner->name_ << std::endl;
        }
    }

private:
	std::weak_ptr<Human> partner_;
    std::string name_;
};

void memory_ok()
{
    using namespace std;

	// RC husband == 1
	auto husband = make_shared<Human>("Jan");
	
	{
		// RC wife == 1
		auto wife = make_shared<Human>("Ewa");
	
		// RC wife bez zmian RC == 1
		husband->set_partner(wife);
	
		// RC husband bez zmian == 1
		wife->set_partner(husband);

        husband->description();
	}  // RC wife spada do zera - obiekt jest kasowany
	
	auto partner1 = husband->partner().lock() ;
	
	if (partner1 == nullptr)
		std::cout << "Obiekt juz zostal usuniety" << std::endl;
			
	try
	{
		auto partner2 = shared_ptr<Human>{husband->partner()} ;
	}
	catch(const std::exception& e)
	{
		std::cout << "Obiekt juz zostal usuniety. " << e.what() << std::endl;
	}
}

int main()
{
    memory_ok();

	system("PAUSE");
}
